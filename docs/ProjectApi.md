# SwaggerClient::ProjectApi

All URIs are relative to *http://localhost:5000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**project_register_post**](ProjectApi.md#project_register_post) | **POST** /project/register | 
[**projects_get**](ProjectApi.md#projects_get) | **GET** /projects | 


# **project_register_post**
> project_register_post(opts)



### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: api_key
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::ProjectApi.new

opts = { 
  body: SwaggerClient::Body.new # Body | 
}

begin
  api_instance.project_register_post(opts)
rescue SwaggerClient::ApiError => e
  puts "Exception when calling ProjectApi->project_register_post: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Body**](Body.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



# **projects_get**
> Project projects_get



### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: api_key
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::ProjectApi.new

begin
  result = api_instance.projects_get
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling ProjectApi->projects_get: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Project**](Project.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



