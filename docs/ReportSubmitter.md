# SwaggerClient::ReportSubmitter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**email** | **String** |  | [optional] 


