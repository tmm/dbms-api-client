# SwaggerClient::Attachment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**body** | **String** |  | [optional] 
**mime_type** | **String** |  | [optional] 
**file_name** | **String** |  | [optional] 
**annex** | **String** |  | [optional] 
**version** | **String** |  | [optional] 


