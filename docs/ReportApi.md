# SwaggerClient::ReportApi

All URIs are relative to *http://localhost:5000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_report**](ReportApi.md#add_report) | **POST** /submit_tpr | Add a new technical progress report submission


# **add_report**
> add_report(body)

Add a new technical progress report submission



### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure API key authorization: api_key
  config.api_key['Authorization'] = 'YOUR API KEY'
  # Uncomment the following line to set a prefix for the API key, e.g. 'Bearer' (defaults to nil)
  #config.api_key_prefix['Authorization'] = 'Bearer'
end

api_instance = SwaggerClient::ReportApi.new

body = SwaggerClient::Report.new # Report | Report object to post


begin
  #Add a new technical progress report submission
  api_instance.add_report(body)
rescue SwaggerClient::ApiError => e
  puts "Exception when calling ReportApi->add_report: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Report**](Report.md)| Report object to post | 

### Return type

nil (empty response body)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



