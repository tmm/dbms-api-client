# SwaggerClient::Report

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**reporting_period** | [**ReportReportingPeriod**](ReportReportingPeriod.md) |  | [optional] 
**submitted_by** | [**ReportSubmittedBy**](ReportSubmittedBy.md) |  | [optional] 
**submitted_on** | **Date** |  | [optional] 
**attachments** | [**Array&lt;Attachment&gt;**](Attachment.md) |  | [optional] 
**comments** | [**Array&lt;Comment&gt;**](Comment.md) |  | [optional] 


