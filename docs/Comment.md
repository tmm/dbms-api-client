# SwaggerClient::Comment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**author** | **String** |  | [optional] 
**body** | **String** |  | [optional] 
**submitted_on** | **String** |  | [optional] 


