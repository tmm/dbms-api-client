# SwaggerClient::Project

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**short_name** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**grant_number** | **String** |  | [optional] 
**start_date** | **String** |  | [optional] 
**end_date** | **String** |  | [optional] 
**grantee_organization** | **String** |  | [optional] 
**funding_amount** | **Integer** |  | [optional] 
**region_id** | **Integer** |  | [optional] 
**duns_number** | **String** |  | [optional] 
**primary_country** | **String** |  | [optional] 


